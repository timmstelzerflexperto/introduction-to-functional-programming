import {pipe} from 'fp-ts/lib/pipeable';
import {flow, not} from 'fp-ts/lib/function';
import {filter, map} from 'fp-ts/lib/Array';

export type User = {firstname: string; lastname: string; born: string};

const exampleUsers: User[] = [
    // Note: Year is in US format
    {firstname: 'John', lastname: 'Smith', born: '09.02.2000'},
    {firstname: 'Barbara', lastname: 'Freud', born: '02.13.1978'},
    {firstname: 'Jeff', lastname: 'Goldbloom', born: '12.24.1950'},
];

/**
 * Exercise #1: Define a function `deriveFullname` so that it takes a user and
 * returns it, extendeded with their full name. Make sure _not_ to mutate the
 * original user.
 *
 * Feel free to check (via `console.log`) along the way against `exampleUsers`.
 *
 * @example
 * deriveFullname({firstname: 'A', lastname: 'B', ...})
 * // => {firstname: 'A', lastname: 'B', ..., fullname: 'A B'}
 */
export const deriveFullname = undefined;

/**
 * Exercise #2: Partially apply `map` with `deriveFullname`, so that your
 * function works on arrays of users.
 */
export const mapDeriveFullname = undefined;

/**
 * Exercise #3: Define a function `deriveAge` that takes a user and
 * returns their age.
 *
 * @example
 * userAge({firstname: 'Jeff', lastname: 'Goldbloom', born: '24.12.1950'})
 * // => 70
 */
export const userAge = undefined;

/**
 * Exercise #4: Define a function `userIsOld` so that it takes a user and
 * asserts that they are old, when over the age of 40. Is this agist as fuck?
 * I read somewhere that algorithms are unbiased, so its not my fault.
 *
 * @example
 * userIsOld({..., born: '24.12.1950'})
 * // => true
 */
export const userIsOld = undefined;

/**
 * Exercise #5: Define a function `userIsNotOld` that inverses the previous function.
 * When `userIsOld` returns `true`, `userIsNotOld` should return `false.
 *
 * Hint: Try using `not`.
 *
 * @example
 * userIsNotOld({..., born: '24.12.1950'})
 * // => false
 */

export const userIsNotOld = undefined;

/**
 * Exercise #6: Partially apply `filter` with `userIsNotOld`, so that your
 * function works on arrays of users.
 */
export const filterIsNotOld = undefined;

/**
 * Exercise #7: If you haven't already, refactor `userIsOld` so that it
 * composes `userAge` and a predicate function, using either `flow` or `pipe`.
 */

/**
 * Exercise #8: Define a function that composes `filterIsNotOld` and
 * `mapDeriveFullname`. The resulting function should take a list of users and
 * return a new list with the users fullnames derived, where the old people are
 * rejected.
 */
export const service = undefined;
