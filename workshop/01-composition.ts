import * as R from 'ramda';
import {pipe} from 'fp-ts/lib/pipeable';
import {flow} from 'fp-ts/lib/function';

// # HIGHER ORDER FUNCTIONS
// aka "functions as values"

const toString = (n: number): string => n.toString();
const numbers = [1, 2, 3, 4];

const example1 = numbers.map(toString);
const example2 = R.map(toString, numbers);

type map = <A, B>(fab: (a: A) => B, as: A[]) => B[];
//         <A, B>
// * Define generics for input and output. `A` and `B` can be
//   literally _any_ type.
//
//               (fab: (a: A) => B, as: A[]) =>
// * Take _any_ function from `A` to `B` and _any_ list of `A`.
//
//                                               B[]
// * Return list of `B`

// `fab` is a "higher order function", because we use it as a parameter to
// another function. We use the function as a _value_, not just as _syntax_.

// # PARTIAL APPLICATION
// "I know most of those arguments already"

//                  A        B
const exclaim = (s: string): string => `${s}!`;
const names = ['Jeff', 'Jim', 'George', 'Barbara', 'Kim', 'Frankie'];
const encouragements = ['Hey', 'Ho', "Let's go"];

// console.log(R.map(exclaim, names));
// console.log(R.map(exclaim, encouragements));

// Partially apply `map` with `exclaim`.
const mapExclaim = (strings: string[]): string[] => R.map(exclaim, strings);
const mapExclaim2 = R.map(exclaim);

// using map ∷ (function, string[]) → string[]
// after partially applying ∷ string[] → string[]
// console.log(mapExclaim(encouragements));

// # CURRYING
// aka: "Give me every argument, one at a time"
// or: "How to turn variadic functions into unary functions.

// type map = <A, B>(fab: (a: A) => B, as: A[]) => B[];
type mapCurried = <A, B>(fab: (a: A) => B) => (as: A[]) => B[];
//                <A, B>
// * Define generics.
//
//                      (fab: (a: A) => B)
// * Take _any_ function from `A` to `B`
//
//                                          => (as: A[])
// * _Return a function_ that takes _any_ list of `A`.
//
//                                                          B[]
// * Return list of `B`

// # COMPOSITION
// aka "glueing functions together to get new functions"

const toArray = (s: string) => [s];

// GOAL: a function that first applies `toString`, then `toArray`:
const f1 = (n: number) => toArray(toString(n));

// compose ∷ (b → c) → (a → b) → a → c
type compose = <A, B, C>(fbc: (b: B) => C) => (fab: (a: A) => B) => (a: A) => C;
// ----------------------------------------------------------------------------
//                      (fbc: (b: B) => C) =>
// 1. FIRST take a function from `B` to `C`
// ----------------------------------------------------------------------------
//                                            (fab: (a: A) => B) =>
// 2. THEN take a function from `A` to `B`
// ----------------------------------------------------------------------------
//                                                                  (a: A) =>
// 3. THEN take any `A`
// ----------------------------------------------------------------------------
//                                                                            C;
// 4. RETURN a `C`
// ----------------------------------------------------------------------------
//
// NOTE: When we invoke compose with the first two arguments, (fbc and fab), we get
// back a function with the signature:
//                                                                  (a: A) => C;

const f2 = R.compose(toArray, toString);
//                   toArray( toString(n) )
// NOTE: Handwavy reason why its "the wrong way": right associativity is useful
// in lazy evaluation (like in Haskell), so compose is right associative.
//
// Fortunately, we don't care, so we'll use compose the "other" way.

// called "pipe" in ramda
type flow = <A, B, C>(fab: (a: A) => B) => (fbc: (b: B) => C) => (a: A) => C;
// flow ∷ (a → b) → (b → c) → a → c

// ramda
const f3 = R.pipe(toString, toArray);
// fp-ts
const f4 = flow(toString, toArray);
const f5 = (n: number) => pipe(n, toString, toArray);

const scream = (s: string) => `${s}!`;

// input example: 14
const f6 = flow(
    // '14'
    toString,
    // ['14']
    toArray,
    // ['14!']
    R.map(s => s + s),
    R.map(s => parseInt(s, 10)),
);

// NOTE: Make sure that your functions are pure (i.e., side effect free)
// to keep composition sane.
