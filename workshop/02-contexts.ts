import * as R from 'ramda';
import {pipe} from 'fp-ts/lib/pipeable';
import {flow} from 'fp-ts/lib/function';

// # FUNCTORS
// map works on more than just arrays

// Consider one of previous examples, where we transformed a `[number]` to
// `[string]`, let's describe the transformation as a rectangle:
//
// [number] > > > > > [string]
//    ^                  v
//    ^                  v
//    ^                  v
//    ^                  v
//    ^                  v
//  number  > > > > > string
//
//  Let's put the corresponding higher-order functions on the edges.
//
// [number] > map(f) > [string]
//    ^                    v
//    ^                    v
//   []                 reduce
//    ^                    v
//    ^                    v
//  number  >    f    >  string
